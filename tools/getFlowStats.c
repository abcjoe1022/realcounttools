#include "libtrace.h"
#include <stdio.h>
#include <inttypes.h>
#include <assert.h>
#include <getopt.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <memory.h>


typedef struct IpInfo{
    struct in_addr ip;
    uint64_t counter;
    float percentage;
    struct IpInfo *next;
} IpInfo;

int packet_num = 0;
int tableEntry = 65536;
uint16_t hashVar[4];
uint64_t prime;
IpInfo *hashTable[65536];
char *InputURI;

void init(){

    prime = (1ULL << 61) - 1;
    int i;
	for( i = 0; i < 4; i++ ){
		hashVar[i] = rand();
    } 
    
    for( i = 0; i < tableEntry ; i++ ){
        hashTable[i] = NULL;
    }
    
}

uint64_t HASH(uint32_t ip){
    uint64_t value;
    value = hashVar[0] * ip * ip * ip ;
    value = value + hashVar[1] * ip * ip ;
    value = value + hashVar[2] * ip ;
    value = value + hashVar[3];
    value = value % prime;
    value = value % tableEntry;
    
    return value;
}

//***
IpInfo * add_node(libtrace_ip_t *ip, libtrace_packet_t *packet){
    IpInfo *new = malloc(sizeof(IpInfo));
    new->ip.s_addr = ip->ip_src.s_addr;
    new->counter = packet->wire_length;
    new->next = NULL;
    
    return new;
}
//***
static void per_packet(libtrace_packet_t *packet)
{
	struct sockaddr_storage addr;
	struct sockaddr *addr_ptr;
	libtrace_ip_t *ip;
	
	uint32_t currentIP;
	uint64_t hashValue;   
    
	addr_ptr = trace_get_source_address(packet, (struct sockaddr *)&addr);
	ip = trace_get_ip(packet);
	
	if(ip == NULL || addr_ptr->sa_family == AF_INET6){
	    return;
	    //printf ("No.%d cap_len = %d, wire_len = %d, ip_len = %d\n", packet_num, packet->capture_length, packet->wire_length, ip->ip_len);
	}
	currentIP = ip->ip_src.s_addr;
    hashValue = HASH(currentIP);
    
    //(if)entry already has something       (Matched)
    //    ---> First matched ---------------------------------> Update node
    //                       |               |
    //                       ---> Try next ---
    //                                       |
    //                                       -----------------> Creat new node
    //(else)empty entry ---> New entry        (No any matched)
    
    if(hashTable[hashValue] != NULL){
        IpInfo *ipinfo = hashTable[hashValue];
        
        //try first
        if(ipinfo->ip.s_addr == currentIP){
            ipinfo->counter = ipinfo->counter + packet->wire_length;
        }else{
            int nodeNum = 1;
            if(ipinfo->next == NULL){
                ipinfo->next = add_node(ip, packet);
            }else{
                ipinfo = ipinfo->next;
                nodeNum++;
                while(ipinfo != NULL){
                    //try next node
                    if(ipinfo->ip.s_addr == currentIP){
                        ipinfo->counter = ipinfo->counter + packet->wire_length;
                          break;
                    }
                    // no any entry matched, creat new node
                    if(ipinfo->next == NULL){
                        ipinfo->next = add_node(ip, packet);
                        break;
                    }  
                    ipinfo = ipinfo->next;
                    nodeNum++;
                }      
            }  
        } 
    }
    else{
        hashTable[hashValue] = add_node(ip, packet);
        //printf("(New entry) %s\thash in\t%d\n", inet_ntoa(hashTable[hashValue]->ip), hashValue);
    }

}

void printResult(){
    IpInfo *ipinfo;
    int i,j;
    int totalIP = 0;
    uint64_t totalByteCount = 0;
    
    IpInfo **arr, *temp;
    
    FILE *fPtr;
    strcat(InputURI, "_Stats.csv");
    fPtr = fopen(InputURI, "w");
    
    for( i = 0; i < tableEntry ; i++) {
        ipinfo = hashTable[i];
        int nodeNum = 0;
        while(ipinfo != NULL){
            //fprintf(fPtr, "%s, %d\n", inet_ntoa(ipinfo->ip), ipinfo->counter);
            totalByteCount = totalByteCount + ipinfo->counter;
            ipinfo = ipinfo->next;
            nodeNum++;
            totalIP++;
        }  
    }
    
    j=0;
    arr = malloc(totalIP * sizeof(IpInfo));
    
    for( i = 0; i < tableEntry ; i++) {
        ipinfo = hashTable[i];
        while(ipinfo != NULL){
             //FIXME ipinfo->percentage = ipinfo->percentage * ipinfo->counter;
             //printf("%s, %.3f, %f\n", inet_ntoa(ipinfo->ip), ipinfo->percentage, ipinfo->counter);
             arr[j] = ipinfo;
             ipinfo = ipinfo->next;
             j++;
        }
    }
    for( i = 0; i < totalIP; i++) {     
        for( j = i; j < totalIP; j++) {
            if( arr[j]->counter > arr[i]->counter) {
                temp = arr[j];
                arr[j] = arr[i];
                arr[i] = temp;
            }
        }
    }
   
    for( i = 0; i < totalIP ; i++) {
        //printf("%s, %.3f, %d\n", inet_ntoa(arr[i]->ip), arr[i]->percentage, arr[i]->counter);
        fprintf(fPtr, "%s," "%" PRIu64 "\n", inet_ntoa(arr[i]->ip), arr[i]->counter);
    }
    
    printf("Total distinct source IP : %d\nTotal byteCount in this trace :" "%" PRIu64 "\n", totalIP, totalByteCount);
    printf("Creat result file: %s\n", InputURI);
}

static void libtrace_cleanup(libtrace_t *trace, libtrace_packet_t *packet) {

        if (trace)
                trace_destroy(trace);

        if (packet)
                trace_destroy_packet(packet);

}

int main(int argc, char *argv[])
{
        /* This is essentially the same main function from readdemo.c */

        libtrace_t *trace = NULL;
        libtrace_packet_t *packet = NULL;
        init();
        
    	/* Ensure we have at least one argument after the program name */
        if (argc < 2) {
                fprintf(stderr, "Usage: %s inputURI\n", argv[0]);
                return 1;
        }

        packet = trace_create_packet();

        if (packet == NULL) {
                perror("Creating libtrace packet");
                libtrace_cleanup(trace, packet);
                return 1;
        }

        trace = trace_create(argv[1]);

        if (trace_is_err(trace)) {
                trace_perror(trace,"Opening trace file");
                libtrace_cleanup(trace, packet);
                return 1;
        }

        if (trace_start(trace) == -1) {
                trace_perror(trace,"Starting trace");
                libtrace_cleanup(trace, packet);
                return 1;
        }

        printf("inputURI: %s \n", argv[1]);
        
        char *temp = argv[1];
        temp = strtok(argv[1], "/");
        
        while (temp != NULL)
        {
            if(strtok (NULL, "/") == NULL){
                InputURI = temp;
            }
            temp = strtok (NULL, "/");
        }      
        //Start
        while (trace_read_packet(trace,packet)>0) {
                packet_num++;
                per_packet(packet);
        }
        
        printResult();

        if (trace_is_err(trace)) {
                trace_perror(trace,"Reading packets");
                libtrace_cleanup(trace, packet);
                return 1;
        }

        libtrace_cleanup(trace, packet);
        return 0;
}

